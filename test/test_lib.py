from unittest import TestCase
from src.simple_package.my_module import greet


class TestLib(TestCase):
    def test_greet(self):
        self.assertEqual(greet(), 'hi there!')
