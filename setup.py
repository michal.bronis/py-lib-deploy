import setuptools
from version import version

with open("README.md", "r") as f:
    long_description = f.read()


setuptools.setup(
    name='simple-package',
    version=version,
    author="Michał Bronis",
    author_email="michal.bronis@gmail.com",
    description="A sample package for deploy testing.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/michal.bronis/py-lib-deploy",
    package_dir={'': 'src'},
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
