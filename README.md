# py-lib-deploy

Tutorial on how to make and deploy python packages with GitLab

## Getting started

Making a distributed package can be split into 4 main steps:

1. preparing **package sources**:
    * creating project
    * coding package logic
    * creating tests
    * preparing files requred for package building
1. **package build** - enabling package to be installed:
    * making the package source or build distributon
1. **deploying package** to packages server:  
enabling installation over internet
    * `PyPI`, 
    * `GCP Artifact Registry` or 
    * `GitLab Package Registry`
1. automating package **build** and **deploy** with `GitLab CI/CD`

## Package Sources
1. setup proper files structure, eg.:  
![image](resources/package_structure.png)


1. prepare `setup.py` file.  
The file contains metainfo about the package (eg.: author name and description) and data used when building the package.
    
   Example file:

    ```
    import setuptools
    from version import version

    with open("README.md", "r") as f:
        long_description = f.read()


    setuptools.setup(
        name='simple-package',
        version=version,
        author="Michał Bronis",
        author_email="michal.bronis@gmail.com",
        description="A sample package for deploy testing.",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/michal.bronis/py-lib-deploy",
        package_dir={'': 'src'},
        packages=setuptools.find_packages(),
        classifiers=[
            "Programming Language :: Python :: 3",
            "Operating System :: OS Independent",
        ],
        install_requires=[]
    )
    ```

    File contents explanation:  
    * `name` name  of the package, this name will be used with `pip install` command.
    * `package_dir` a dictionary of `package: dir` entries. Defines source directory lay out. `''` stands for the root directory of project.  
    The `{'': 'src'}` entry lets you import installed package with:  
        ```
        import simple_package
        ```
        instead of:  
        ```
        import src.simple_package
        ```
    * `packages` list of package names. All modules found in listed packages will be built and installed.  
    Here we let the `setuptools` discover all packages.
    * `install_requires` dependencies of the package, those will be installed along with the package with `pip install` command.  
    This is not the same as requirements defned in `requirements.txt` file. The later defines dependiencies for whole project. See [install_requires vs requirements files](https://packaging.python.org/discussions/install-requires-vs-requirements/).
    * `version` version of package. It will be used when for example publishing the package to `PyPI`


1. package `__init__.py` file  
The `__init__.py` file serves two purposes:  
    * it tells that `simple_package` directory is as package  
    For this purpose it is enough that you create empty `__init__.py` file
    * lets you define logic that will be executed when the package is imported  
    In our example it contains module import:
        ```
        from simple_package import my_module
        ```
        Thanks to that import we can use the package as follows:
        ```
        import simple_package as sp

        sp.greet()
        ```
        Without the need to refer to `my_module` in the function call.


## Package building

1. A package created with instructions from above paragraph can be **installed locally** with:
    ```
    pip install -e .
    ```

    However, this method does not allow for (easy) package sharing.
1. **source distribution**  
Create `source distribution` with following command:
    ```
    python setup.py sdist
    ```

    It creates source distribution in dist folder:  
    `dist/simple-package-0.0.1.tar.gz`
1. **binary distribution**  
Create binary distribution with following:
    ```
    python setup.py bdist
    ```

    It creates source distribution in dist folder:  
    `dist/simple-package-0.0.1.linux-x86_64.tar.gz`
1. **wheel distribution**  
Creates binary distribution in `wheel` format that can be uploaded to `PyPI`.
    ```
    python setup.py bdist_wheel
    ```

    It creates source distribution in dist folder:  
    `dist/simple_package-0.0.1-py3-none-any.whl`

## Package deployment - `GitLab`
Packages can be deployed to `GitLab` package registry.

1. create token with scope set to `api`   
See [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

1. edit the `~/.pypirc` file, add:
    ```shell
    [distutils]
    index-servers =
        gitlab

    [gitlab]
    repository = https://gitlab.com/api/v4/projects/<project_id>/packages/pypi
    username = <your_personal_access_token_name>
    password = <your_personal_access_token>

    ```
1. push packages to `GitLab` `PyPI`
    ```
    python3 -m twine upload --repository gitlab dist/*
    ```

1. install from `GitLab`
    ```
    pip install simple-package --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/29042266/packages/pypi/simple
    ```